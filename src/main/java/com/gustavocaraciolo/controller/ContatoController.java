/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gustavocaraciolo.controller;

import com.gustavocaraciolo.models.Contato;
import com.gustavocaraciolo.repositories.ContatoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Gustavo
 */
@RestController
@RequestMapping(value = "/api")
public class ContatoController {
    private static final Logger logger = LoggerFactory.getLogger(ContatoController.class);

    @Autowired
    private ContatoRepository contatoRepository;
    
    @RequestMapping(value = "/contato", method = RequestMethod.POST)
    @Transactional
    public ResponseEntity<?> enviarContato(@RequestBody Contato contato) {
        contatoRepository.save(contato);
        contato = contatoRepository.getContatoByName(contato.getNome());
        return new ResponseEntity(contato, HttpStatus.OK);
    }
}
