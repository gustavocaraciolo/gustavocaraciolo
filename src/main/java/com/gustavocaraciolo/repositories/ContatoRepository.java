/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gustavocaraciolo.repositories;

import com.gustavocaraciolo.models.Contato;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gustavo
 */
@Service
@Transactional
public interface ContatoRepository extends JpaRepository<Contato, Long>{
    @Query("select u from Contato u where u.nome = :nome")
    public Contato getContatoByName(@Param("nome")String nome);

}