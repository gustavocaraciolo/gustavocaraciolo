'use strict';
var app = angular.module('gustavoControllers',['ngCookies']);
app.controller('ContatoController', ['$scope','$location','AuthenticationService','RegisterService',
    function($scope, $location, AuthenticationService, RegisterService){
    AuthenticationService.clearCredentials(); // reset login status

    $scope.enviarContato = function(){
        $scope.dataLoading = true;
        var $user = {

                nome:$scope.nome,
                sobrenome:$scope.sobrenome,
                email:$scope.email,
                titulo:$scope.titulo,
                mensagem:$scope.mensagem
            };

        RegisterService.register($user, function(response,status) {
            if(status==200) {
                $location.path('/');
            }
            else{
                console.log(response.status);
                $scope.error = response.message;
            }
            $scope.dataLoading = false;
        });
    }
}]);