'use strict';

var app = angular.module('gustavo',[
    'app.config',
    'ngRoute',
    'ngCookies',
    'gustavoControllers',
    'gustavoServices'
]);

app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
        when('/contato', {
            templateUrl: 'contact.html',
            controller: 'ContatoController'
        }).
        otherwise({
            redirectTo: '/'
        });
    }
]);
angular.module('app.config',[])
    .value('app.config', {
        apiBasePath:'http://localhost:8090/api/'
    });

/*app.run(['$rootScope', '$location', '$cookieStore', '$http',
    function ($rootScope, $location, $cookieStore, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
            $http.defaults.headers.common['AccountType'] = $rootScope.globals.currentUser.accounttype;
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in
            if (($location.path() !== '/login' && $location.path() !== '/register') && !$rootScope.globals.currentUser) {
                $location.path('/login');
            }
        });
    }])*/

//$cookieStore.get('globals').currentUser.accountType
