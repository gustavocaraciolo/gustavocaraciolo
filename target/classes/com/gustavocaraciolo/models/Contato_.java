package com.gustavocaraciolo.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-02-09T20:33:57")
@StaticMetamodel(Contato.class)
public class Contato_ { 

    public static volatile SingularAttribute<Contato, String> mensagem;
    public static volatile SingularAttribute<Contato, String> titulo;
    public static volatile SingularAttribute<Contato, String> nome;
    public static volatile SingularAttribute<Contato, Long> id;
    public static volatile SingularAttribute<Contato, String> sobrenome;
    public static volatile SingularAttribute<Contato, String> email;

}